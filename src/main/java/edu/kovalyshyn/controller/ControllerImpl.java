package edu.kovalyshyn.controller;

import edu.kovalyshyn.model.RoboModel;
import edu.kovalyshyn.model.RobotStrong;


public class ControllerImpl implements Controller {

  private RoboModel roboModel;

  public ControllerImpl() {
    roboModel = new RobotStrong("Log4j", "all log msg");
  }

  @Override
  public void speak() {
    roboModel.speak();
  }

  @Override
  public void walk() {
    roboModel.walk();
  }

  @Override
  public void shoot() {
    roboModel.shoot();

  }

  @Override
  public void showLogLevel() {
    roboModel.showLogLevel();

  }
}
