package edu.kovalyshyn.controller;

public interface Controller {
  void speak();

  void walk();

  void shoot();

  void showLogLevel();

}
