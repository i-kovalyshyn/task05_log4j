package edu.kovalyshyn.model;

public interface RoboModel {

  void speak();

  void walk();

  void shoot();

  void showLogLevel();


}
