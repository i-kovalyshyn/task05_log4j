package edu.kovalyshyn.model;

import org.apache.logging.log4j.*;

public class RobotStrong implements RoboModel {

  private static Logger logger1 = LogManager.getLogger(RobotStrong.class);
  private String name;
  private String level;


  public RobotStrong(String name, String level) {
    this.name = name;
    this.level = level;
  }

  public void speak() {
    System.out.println("speak");
    logger1.info("This is an info message from " + name);
    logger1.trace("This is a trace message"+level);
    logger1.debug("<<<=====This is a debug message");
    logger1.warn("This is a warn message" + name);
    logger1.error("<<<<==This is an error message");
    logger1.fatal("This is a fatal message"+level);


  }

  public void walk() {
    logger1.error("<<<<<====This is a warn message from "
        + name + ": I can`t walk");
  }

  public void shoot() {
    logger1.warn("This is a warn message from " + name + ": I can shoot");
  }

  public void showLogLevel() {
    logger1.warn("This is a trace message from " + name + ": "
        + "file will  be overwritten after reaching the size 1MB");
  }

}
