package edu.kovalyshyn.view;

import org.apache.logging.log4j.*;

import edu.kovalyshyn.controller.Controller;
import edu.kovalyshyn.controller.ControllerImpl;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {


  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  private static Logger log = LogManager.getLogger(MyView.class);

  public MyView() {
    controller = new ControllerImpl();
    menu = new LinkedHashMap<>();
    menu.put("1", " 1 - Write Log4j2 to files");
     menu.put("Q", " Q - EXIT");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);

    log.trace("-----------------from MyView------------------");
  }

  private void pressButton1() {
    controller.speak();
    controller.shoot();
    controller.showLogLevel();
    controller.walk();

  }


  private void outputMenu() {
    System.out.println("\n MENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
        log.trace("-EXIT- This is a trace message");
      }
    } while (!keyMenu.equals("Q"));
  }
}


